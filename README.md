# ASP.NET Core Web API - Controller Testing Activity

This project provides a hands-on activity for learning how to implement unit tests for controllers in an ASP.NET Core Web API application, using Moq for mocking dependencies.

## Project Overview

The project is a simple ASP.NET Core Web API targeting .NET 6, designed to manage products. It includes a Product model, service, repository, and a controller to handle CRUD operations.

### Key Components

- `Product` Model: Located in the `Models` folder.
- `IProductService` & `ProductService`: Service layer for handling business logic.
- `ProductsController`: Controller with CRUD operations for products.
- `ProductServiceTests`: Completed unit tests for the `ProductService`.

## Setup

1. Clone the repository to your local machine.
2. Open the project in Visual Studio or your preferred IDE.
3. Ensure .NET 6 SDK is installed.
4. Build the project to restore NuGet packages.

## Activity: Implementing Controller Tests

### Objective

Implement unit tests for the `ProductsController`, focusing on isolating the controller's behavior and using Moq for mocking the service layer.

### Instructions

1. Review `ProductServiceTests` to understand how the service tests are structured for isolation and specific functionalities testing.
2. **Set Up `ProductsControllerTests`**:
   - Create `ProductsControllerTests` in the `Tests` folder.
   - Use Moq to mock `IProductService`.
   - Initialize `ProductsController` with the mocked service.
3. Write tests for each endpoint, handling both successful operations and error scenarios.
4. Apply best practices for test isolation and independence. Include descriptive comments.
5. After implementation, discuss mocking in unit tests and the difference between service and controller tests.

### Tests to Implement

- **GetAllProducts Endpoint Tests**
  - `GetAllProducts_ReturnsAllProducts`
  - `GetAllProducts_ReturnsEmptyListWhenNoProducts`
- **GetProduct Endpoint Tests**
  - `GetProduct_ExistingId_ReturnsProduct`
  - `GetProduct_NonExistingId_ReturnsNotFound`
- **PostProduct Endpoint Tests**
  - `PostProduct_ValidProduct_ReturnsCreatedAtAction`
  - `PostProduct_InvalidProduct_ReturnsBadRequest`
- **PutProduct Endpoint Tests**
  - `PutProduct_MatchingId_UpdatesProduct`
  - `PutProduct_MismatchingId_ReturnsBadRequest`
  - `PutProduct_NonExistingId_ReturnsNotFound`
- **DeleteProduct Endpoint Tests**
  - `DeleteProduct_ExistingId_DeletesProduct`
  - `DeleteProduct_NonExistingId_ReturnsNotFound`

## Mock Setup Guidance

- Set up `IProductService` mock for each test, relevant to the test scenario.
- Focus on asserting response types and data correctness.
- Ensure mocks are specific to each test for isolation.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
