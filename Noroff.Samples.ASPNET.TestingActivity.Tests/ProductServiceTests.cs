using Microsoft.EntityFrameworkCore;
using Moq;
using Noroff.Samples.ASPNET.TestingActivity.Data;
using Noroff.Samples.ASPNET.TestingActivity.Services;

namespace Noroff.Samples.ASPNET.TestingActivity.Tests
{
    public class ProductServiceTests
    {
        private ApplicationDbContext _context;
        private IProductService _service;

        public ProductServiceTests()
        {
            // Constructor is kept minimal. Actual initialization will be done in a separate method.
            InitializeContext();
        }

        private void InitializeContext()
        {
            // Creating a new database context for each test ensures isolation.
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Unique DB name for isolation
                .Options;

            _context = new ApplicationDbContext(options);
            SeedDatabase(); // Call the method to seed the database
            _service = new ProductService(_context); // Create our service with our test database.
        }

        private void SeedDatabase()
        {
            // Seed the database with initial data, if necessary.
            if (!_context.Products.Any())
            {
                _context.Products.AddRange(new List<Product>
                {
                    new Product { Name = "Test Product 1", Price = 10.00M },
                    new Product { Name = "Test Product 2", Price = 20.00M }
                });
                _context.SaveChanges();
            }
        }

        // Helper method to dynamically create test data (we dont rely on remembering IDs)
        private Product CreateTestProduct(string name, decimal price)
        {
            var product = new Product { Name = name, Price = price };
            _context.Products.Add(product);
            _context.SaveChanges();
            return product;
        }

        // We will implement the IDisposable interface to ensure that resources are cleaned up.
        private void Dispose()
        {
            _context.Database.EnsureDeleted(); // Deletes the in-memory database.
            _context.Dispose(); // Disposes of the database context.
        }

        [Fact]
        public async Task GetAllProductsAsync_WhenCalled_ReturnsAllProducts()
        {
            InitializeContext(); // Ensure a fresh context for the test

            // Act
            var result = await _service.GetAllProductsAsync();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count()); // Expecting 2 products as seeded in the database
        }


        [Fact]
        public async Task AddProductAsync_ValidProduct_AddsProduct()
        {
            InitializeContext(); // Ensure a fresh context for this test

            // Arrange
            var newProduct = new Product { Name = "Test Product 3", Price = 30.00M };

            // Act
            await _service.AddProductAsync(newProduct);
            var addedProduct = await _context.Products.FindAsync(newProduct.Id);

            // Assert
            Assert.NotNull(addedProduct);
            Assert.Equal("Test Product 3", addedProduct.Name);
        }


        [Fact]
        public async Task UpdateProductAsync_ProductExists_UpdatesProduct()
        {
            InitializeContext(); // Fresh context for isolation

            // Arrange
            var existingProduct = CreateTestProduct("Existing Product", 15.00M); // Using dynamic data creation

            // Act
            existingProduct.Name = "Updated Product";
            await _service.UpdateProductAsync(existingProduct);
            var updatedProduct = await _context.Products.FindAsync(existingProduct.Id);

            // Assert
            Assert.NotNull(updatedProduct);
            Assert.Equal("Updated Product", updatedProduct.Name);
        }

        [Fact]
        public async Task UpdateProductAsync_ProductDoesNotExist_DoesNotUpdate()
        {
            InitializeContext(); // Fresh context

            // Arrange
            var nonExistingProduct = new Product { Id = 99, Name = "Non-Existing Product", Price = 100.00M };

            // Act
            await _service.UpdateProductAsync(nonExistingProduct);
            var updatedProduct = await _context.Products.FindAsync(99);

            // Assert
            Assert.Null(updatedProduct);
        }

        [Fact]
        public async Task DeleteProductAsync_ProductExists_DeletesProduct()
        {
            InitializeContext(); // Fresh context

            // Arrange
            var productToDelete = CreateTestProduct("Product To Delete", 25.00M);

            // Act
            await _service.DeleteProductAsync(productToDelete.Id);
            var deletedProduct = await _context.Products.FindAsync(productToDelete.Id);

            // Assert
            Assert.Null(deletedProduct);
        }

        [Fact]
        public async Task DeleteProductAsync_ProductDoesNotExist_DoesNothing()
        {
            InitializeContext(); // Fresh context

            // Act
            await _service.DeleteProductAsync(99); // ID 99 does not exist
            var productCount = await _context.Products.CountAsync();

            // Assert
            Assert.Equal(2, productCount); // Assuming initial seed had 2 products
        }
    }

}