﻿using Noroff.Samples.ASPNET.TestingActivity.Data;

namespace Noroff.Samples.ASPNET.TestingActivity.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
        Task<Product> GetProductByIdAsync(int productId);
        Task AddProductAsync(Product product);
        Task UpdateProductAsync(Product product);
        Task DeleteProductAsync(int productId);
    }

}
