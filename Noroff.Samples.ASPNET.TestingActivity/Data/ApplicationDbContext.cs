﻿using Microsoft.EntityFrameworkCore;

namespace Noroff.Samples.ASPNET.TestingActivity.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }

        public DbSet<Product> Products { get; set; }
    }

}
